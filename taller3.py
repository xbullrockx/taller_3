#procesamiento de datos para el api
#primero se hace la importacion de las librerias 
#importamos el fast api

from typing import Union
from fastapi import FastAPI
import mook
 
app = FastAPI()

def buscador():
    # la variable indices es el diccionario que se creo para las tutelas
    indices = {}
    #llaves son las palabras claves con las que se refina la busqueda.
    llaves = ["violencia", "estafa", "alimentos", "salud","pensión", "medicinas"]
    for tutela in mook.tutela:
        #usar split para partir las palabras del resumen de las tutelas en el archivo mook
        words = tutela["resumen"].split()
        #este ciclo es el encargado de indexar cuando las palabras estan en el filtro.
        for word in words:
            if word in llaves:
                if word in indices:
                    #cuando la palabra ya existe en los indices, se le agrega la nueva tutela al valor anterios usando append
                    indices[word].append(tutela)
                else:
                    indices[word] = [tutela]      
    return indices

@app.get("/")
def read_root(palabra: str):
    #crea la variable que retorna el buscador
    diccionario = buscador()
    return {"las tutelas encontradas son ": diccionario.get("palabra")}

